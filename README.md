Setup another project to run the browser sync proxy, preferably on a folder close to your Polymer app folder

* `npm init` inside your project directory to create a package.json file
* `npm install browser-sync --save-dev` in the same directory
* Add a start script to package.json:

  "scripts": {
    "test": "polymer test",
    "start": "browser-sync start --proxy localhost:8081 --port 8082 --files '../poc/poly2-patient-app/src/*.html, ../poc/poly2-patient-app/src/*.js, ../poc/poly2-patient-app/images/*' "

   NOTE: Update the browser-sync options to suit to your needs, make sure to point the files to the folder of the files of your Polymer app.

4) `npm start`